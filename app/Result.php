<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'points', 'user_id', 'approved', 'intents'
    ];
}
