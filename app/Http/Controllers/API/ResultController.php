<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Result;
use Validator;

class ResultController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $results = Result::all();

        return $this->sendResponse($results->toArray(), 'Products retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'points' => 'required',
            'user_id' => 'required', 
            'approved' => 'required', 
            'intents' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->erros());
        }

        $result = Result::create($input);

        return $this->sendResponse($result->toArray(), 'Product created succesfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id) {
        $result = Result::find($id);

        if (is_null($result)) {
            return $this->sendError('Result not found');
        }

        return $this->sendResponse('Result retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Result $result) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'points' => 'required',
            'user_id' => 'required', 
            'approved' => 'required', 
            'intents' => 'required'
        ]);

        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());       

        }

        $result->points = $input['points'];
        $result->user_id = $input['user_id'];
        $result->approved = $input['approved'];
        $result->intents = $input['intents'];
        $result->save();

        return $this->sendResponse($result->toArray(), 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     * 
     */
    public function destroy(Result $result) {
        $result->delete();

        return $this->sendResponse($result->toArray(), 'Result deleted successfully');
    }

}
