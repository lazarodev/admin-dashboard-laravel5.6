<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller isntace
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('myHome');
    }

    /**
     * Show my users page.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        return view('myUsers');
    }

}
